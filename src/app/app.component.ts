import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from './modal/modal.component'
import { RequestValidation } from './model/requestValidation.model';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { BanxicoInstitucionService } from './service/banxicoInstitucion.service';
import { KeyValue } from './model/keyValue.model';
import { MatSnackBar } from '@angular/material';
import { MessageComponent } from './message/message.component';
import { Beneficiario } from './model/Beneficiario.model';
import { Ordenante } from './model/ordenante.model';
import { ConsultaOperacionService } from './service/consultaOperacion.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'material';
  public requestValidation: RequestValidation;
  public instituciones: Array<KeyValue>;
  public institucionesMISPEI: Array<KeyValue>;
  public beneficiario: Beneficiario;
  public ordenante: Ordenante;
  constructor(
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    private banxicoInstitucionService: BanxicoInstitucionService,
    private consultaOperacionService: ConsultaOperacionService) {
    this.clear()
  }
  clear() {
    this.requestValidation = new RequestValidation('T', '', '', '', '', '', '0', '', 'c', '1')
  }
  isValid() {
    return this.requestValidation.fecha
      && this.requestValidation.tipoCriterio
      && this.requestValidation.criterio
      && this.requestValidation.emisor
      && this.requestValidation.receptor
      && this.requestValidation.cuenta
      && this.requestValidation.monto
  }
  search() {
    if (this.isValid()) {
      this.openDialog()
    } else {
      this.openSnackBar()
    }
  }
  search2() {
    if (this.isValid()) {
      setTimeout(() => {
        this.getOperation(0)
      }, 2000)

    } else {
      this.openSnackBar()
    }
  }
  openDialog() {
    this.dialog.open(ModalComponent, {
      width: '950px',
      height: '600px',
      data: this.requestValidation
    });
  }
  openSnackBar() {
    this._snackBar.openFromComponent(MessageComponent, {
      duration: 5000,
    });
  }
  onDateChange(event: MatDatepickerInputEvent<Date>) {
    this.requestValidation.fecha = new Date(event.value).toISOString()
      .split('T')[0].split("-").reverse().join("-")
    this.banxicoInstitucionService.getInstituciones(this.requestValidation.fecha)
      .subscribe(
        result => {
          let data: any = result
          this.fillInstitutions(data)
          this.fillInstitutionsMISPEI(data)
        },
        error => { }
      )
  }

  private getOperation(tries: number) {
    this.consultaOperacionService
      .getOperation(this.requestValidation)
      .subscribe(
        result => {
          let data: any = result
          if (data.claveRastreo) {
            this.ordenante = data.Ordenante as Ordenante
            this.beneficiario = data.Beneficiario as Beneficiario
          } else {
            if (tries < 3) {
              setTimeout(() => {
                this.getOperation(tries++)
              }, 2000)
            }
          }
        },
        error => {
          console.error(error)
        }
      )
  }

  fillInstitutions(data: any) {
    this.instituciones = []
    this.instituciones = data.instituciones.map(e => {
      return new KeyValue(e[0], e[1])
    })
    this.requestValidation.emisor = this.instituciones[0].key
  }

  fillInstitutionsMISPEI(data: any) {
    this.institucionesMISPEI = []
    this.institucionesMISPEI = data.institucionesMISPEI.map(e => {
      return new KeyValue(e[0], e[1])
    })
    this.requestValidation.receptor = this.institucionesMISPEI[0].key
  }

  getInstitutions() {
    if (!this.instituciones || this.instituciones.length === 0) return []
    return this.instituciones
  }

  getInstitutionsMISPEI() {
    if (!this.institucionesMISPEI || this.institucionesMISPEI.length === 0) return []
    return this.institucionesMISPEI
  }
}

