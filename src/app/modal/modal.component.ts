import { Component, OnInit, Input, Inject } from '@angular/core';
import { ConsultaOperacionService } from '../service/consultaOperacion.service';
import { Beneficiario } from '../model/Beneficiario.model';
import { Ordenante } from '../model/ordenante.model';
import { RequestValidation } from '../model/requestValidation.model';
import { MAT_DIALOG_DATA } from '@angular/material'
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  public beneficiario: Beneficiario;
  public ordenante: Ordenante;


  constructor(
    @Inject(MAT_DIALOG_DATA) public requestValidation: RequestValidation,
    private consultaOperacionService: ConsultaOperacionService) { }

  ngOnInit() {
    console.log(this.requestValidation)
    setTimeout(() => {
      this.getOperation(0)
    }, 2000)
    // this.getOperation()
  }
  // 202003184015603264
  private getOperation(tries: number) {
    this.consultaOperacionService
      .getOperation(this.requestValidation)
      .subscribe(
        result => {
          let data: any = result
          if (data.claveRastreo) {
            this.ordenante = data.Ordenante as Ordenante
            this.beneficiario = data.Beneficiario as Beneficiario
          } else {
            if (tries < 3) {
              setTimeout(() => {
                this.getOperation(tries++)
              }, 2000)
            }
          }
        },
        error => {
          console.error(error)
        }
      )
  }

}
