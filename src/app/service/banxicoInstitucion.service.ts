import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class BanxicoInstitucionService {
  private url: string;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'DELETE, POST, GET, OPTIONS',
      'Access-Control-Allow-Headers': 'Origin, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
    })
  };

  constructor(public http: HttpClient) {
    this.url = 'http://localhost:8080/cepservice/banxico/institution'
  }

  public getInstituciones(fecha: string): Observable<any> {
    return this.http.get(this.url, {
      headers: this.httpOptions.headers,
      params: { fecha }
    })
  }

}
