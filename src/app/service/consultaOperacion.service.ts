import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RequestValidation } from '../model/requestValidation.model';

@Injectable()
export class ConsultaOperacionService {
  private url: string;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'DELETE, POST, GET, OPTIONS',
      'Access-Control-Allow-Headers': 'Origin, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
    })
  };
  constructor(public http: HttpClient) {
    this.url = 'http://localhost:8080/cepservice/consultas/operacion'
  }

  public getOperation(requestValidation: RequestValidation): Observable<any> {
    // {
    //   "tipoCriterio": "T",
    //   "fecha": "18-03-2020",
    //   "criterio": "202003184015603264",
    //   "emisor": "40156",
    //   "receptor": "40012",
    //   "cuenta": "012180001466279780",
    //   "receptorParticipante": 0,
    //   "monto": 3.21,
    //   "captcha": "c",
    //   "tipoConsulta": 1
    // }
    return this.http.post(this.url, requestValidation, this.httpOptions)
  }
}
