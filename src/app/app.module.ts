import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Component } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { MatButtonModule, MatToolbarModule } from '@angular/material'
import { MaterialModule } from './material.module'
import { MatDialogModule } from '@angular/material/dialog';
import { ModalComponent } from './modal/modal.component';
import { HttpClientModule } from '@angular/common/http';
import { ConsultaOperacionService } from './service/consultaOperacion.service';
import { BanxicoInstitucionService } from './service/banxicoInstitucion.service';
import { ReactiveFormsModule, FormGroupName, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MessageComponent } from './message/message.component';

@NgModule({
  declarations: [
    AppComponent,
    ModalComponent,
    MessageComponent
  ],
  entryComponents: [ModalComponent, MessageComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatDialogModule,
    HttpClientModule,
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule
  ],
  providers: [
    ConsultaOperacionService,
    BanxicoInstitucionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}



