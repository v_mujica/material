import { Beneficiario } from './beneficiario.model';
import { Ordenante } from './ordenante.model';
Ordenante
Beneficiario
export class SPEITercero {
  constructor(
    public fechaOperacion: string,
    public claveRastreo: string,
    public cadenaCDA: string,
    public hora: string,
    public numeroCertificado: string,
    public sello: string,
    public ClaveSPEI: string,
    public ordenante: Ordenante,
    public beneficiario: Beneficiario,
  ) { }
}
