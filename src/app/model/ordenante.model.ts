export class Ordenante {
  constructor(
    public Nombre: string,
    public BancoEmisor: string,
    public TipoCuenta: string,
    public Cuenta: string,
    public RFC: string,
  ) { }
}
