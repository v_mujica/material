export class RequestValidation {
  constructor(
    public tipoCriterio: string,
    public fecha: string,
    public criterio: string,
    public emisor: string,
    public receptor: string,
    public cuenta: string,
    public receptorParticipante: string,
    public monto: string,
    public captcha: string,
    public tipoConsulta: string,
  ) { }
}
