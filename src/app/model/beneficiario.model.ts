export class Beneficiario {
  constructor(
    public Nombre: string,
    public IVA: string,
    public MontoPago: string,
    public TipoCuenta: string,
    public Cuenta: string,
    public Concepto: string,
    public RFC: string,
    public BancoReceptor: string,
  ) { }
}
